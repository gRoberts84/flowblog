-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 02, 2013 at 01:54 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `flowblog`
--
CREATE DATABASE IF NOT EXISTS `flowblog` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `flowblog`;

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `ArticleID` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(200) NOT NULL,
  `Content` text NOT NULL,
  `UserID` int(11) NOT NULL,
  `Created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Published` timestamp NULL DEFAULT NULL,
  `LastUpdated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `Removed` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ArticleID`),
  KEY `UserID` (`UserID`),
  KEY `Published` (`Published`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`ArticleID`, `Title`, `Content`, `UserID`, `Created`, `Published`, `LastUpdated`, `Removed`) VALUES
(1, 'Bacon loveliness', '<p>Bacon ipsum dolor sit amet corned beef ham pancetta strip steak flank filet mignon pig hamburger kielbasa. Ribeye turkey t-bone strip steak chuck salami. Frankfurter short ribs hamburger meatloaf cow kevin. Salami sirloin ball tip, fatback turkey pancetta ham kevin beef ribs swine venison shankle t-bone turducken.</p>\r\n<hr class="break" />\r\n<p>Spare ribs chicken fatback short loin. Strip steak tenderloin pig, prosciutto biltong fatback ribeye tongue boudin tri-tip bresaola doner meatloaf chuck. Pork loin ribeye bacon, fatback turkey tongue shank. Brisket pork belly venison pork boudin capicola shankle rump kevin tongue pork chop salami.</p>\r\n<p>Boudin strip steak tenderloin fatback, leberkas pork chop tail doner bresaola bacon tri-tip. Sirloin jerky pork chop filet mignon capicola venison andouille pig ground round short loin shoulder beef. Shoulder tail t-bone beef ribs doner ribeye leberkas jowl ground round shankle. Kielbasa jerky shoulder salami drumstick sausage beef ball tip leberkas cow frankfurter tri-tip.</p>\r\n<p>Kielbasa tongue tenderloin turducken beef corned beef ribeye ball tip. Turkey capicola prosciutto, tenderloin salami shankle pig ham cow pork belly kielbasa biltong pork loin swine. Meatball ball tip short ribs tenderloin boudin. Leberkas beef pig tail, drumstick pork shankle salami jowl brisket. Drumstick hamburger tail ball tip, pork prosciutto fatback kevin sausage flank pork chop jowl. Jerky filet mignon swine strip steak short loin, chuck ribeye hamburger bresaola sausage. Sausage jerky sirloin tenderloin turducken jowl pastrami short loin beef ribs ham hock turkey pork flank.</p>\r\n<p>Tongue pig kevin spare ribs fatback. Sausage pig strip steak, turkey drumstick jerky venison. Ham hock frankfurter hamburger, pork shank chuck kevin short loin drumstick. Strip steak pig swine ribeye, pork belly jowl tongue sirloin pork loin t-bone chuck drumstick flank shoulder. Swine shoulder t-bone fatback shank pork loin. Ribeye chicken meatloaf swine ham spare ribs pancetta t-bone.</p>', 1, '2013-10-02 13:30:08', '2013-10-02 13:30:00', NULL, NULL),
(2, 'Caffeine kick!', '<p>Redeye strong id caramelization filter mocha aftertaste roast variety, single origin single shot barista affogato coffee to go foam. In qui, half and half, aftertaste, froth dripper robust strong spoon, aroma viennese white cream cultivar single shot foam. Black, aged skinny, macchiato, gal&atilde;o, irish barista sweet, java plunger pot caramelization blue mountain skinny, cultivar, in fair trade qui coffee eu java. Bar, filter, sit, irish, crema, et, redeye plunger pot, bar espresso qui seasonal, macchiato cup foam, and qui extra con panna organic. Americano, grounds filter aroma grounds caf&eacute; au lait qui kopi-luwak aftertaste to go variety caffeine milk gal&atilde;o pumpkin spice grounds grinder, robust irish black milk grounds java. Est grinder, doppio, coffee, organic, decaffeinated sugar organic java, percolator espresso, skinny, so, doppio strong, est, coffee beans ut macchiato percolator.</p>\r\n<p>Mocha seasonal irish cream, cinnamon caf&eacute; au lait plunger pot, ut in robusta bar froth white sugar. Seasonal viennese medium rich, sweet, affogato java, coffee in, lungo, dripper barista to go trifecta instant iced skinny. Mazagran cortado carajillo beans, cultivar, single origin milk, single origin rich, black id dripper viennese wings extraction pumpkin spice single origin caffeine to go. Brewed beans shop macchiato roast ut chicory, fair trade siphon pumpkin spice aged caffeine frappuccino roast foam redeye. Latte lungo cultivar flavour latte dark to go lungo affogato, roast white whipped, carajillo beans white milk so cup saucer. Whipped, est doppio cortado java americano at percolator flavour, qui americano grinder aromatic arabica.</p>\r\n<hr class="break" />\r\n<p>Cream irish cultivar, cortado so, affogato, percolator eu est, trifecta roast caffeine doppio, caffeine, lungo, latte, medium blue mountain dark ut steamed single shot. Eu cappuccino acerbic, extraction, in milk blue mountain, americano single shot americano roast et caffeine coffee extraction con panna grounds aftertaste cream steamed. Turkish, aromatic fair trade crema arabica, rich, half and half grounds single shot strong, mazagran at, iced filter cappuccino macchiato, sweet cup caffeine turkish frappuccino. Extraction con panna coffee espresso eu shop organic strong spoon robusta cappuccino affogato spoon. To go, crema, viennese single origin aroma cup sweet acerbic, frappuccino ristretto turkish fair trade, flavour coffee body filter, in decaffeinated barista ut grounds dripper. Viennese, decaffeinated ut, redeye, trifecta, spoon siphon, grinder decaffeinated ut, foam strong, fair trade, lungo sugar, variety ristretto doppio extraction whipped.</p>\r\n<p>Aroma gal&atilde;o half and half chicory java redeye sit kopi-luwak foam beans sit sugar doppio, variety robust a sweet sit eu rich that. Eu, caf&eacute; au lait crema, breve body crema, cortado, cream siphon redeye mug, half and half plunger pot grinder gal&atilde;o, cappuccino, brewed affogato dark extra blue mountain whipped strong. Id redeye, sit bar a, filter latte white, sit skinny plunger pot body and fair trade. Qui, beans eu roast grounds skinny shop fair trade aged, eu, rich eu decaffeinated crema aftertaste espresso cortado carajillo bar breve whipped. Kopi-luwak coffee, qui latte to go variety mocha medium ut froth redeye, blue mountain iced arabica spoon carajillo caf&eacute; au lait. Grinder chicory est kopi-luwak, latte as, robusta latte seasonal white con panna, mug milk, body cup medium frappuccino est cultivar.</p>', 1, '2013-10-02 13:50:42', '2013-10-02 13:30:00', NULL, NULL),
(3, 'To infinity and beyond', '<p>A Chinese tale tells of some men sent to harm a young girl who, upon seeing her beauty, become her protectors rather than her violators. That''s how I felt seeing the Earth for the first time. I could not help but love and cherish her.</p>\r\n<p>Space, the final frontier. These are the voyages of the Starship Enterprise. Its five-year mission: to explore strange new worlds, to seek out new life and new civilizations, to boldly go where no man has gone before.</p>\r\n<p>Never in all their history have men been able truly to conceive of the world as one: a single sphere, a globe, having the qualities of a globe, a round earth in which all the directions eventually meet, in which there is no center because every point, or none, is center &mdash; an equal earth which all men occupy as equals. The airman''s earth, if free men make it, will be truly round: a globe in practice, not in theory.</p>\r\n<hr class="break" />\r\n<p>Here men from the planet Earth first set foot upon the Moon. July 1969 AD. We came in peace for all mankind.</p>\r\n<p>There can be no thought of finishing for &lsquo;aiming for the stars.&rsquo; Both figuratively and literally, it is a task to occupy the generations. And no matter how much progress one makes, there is always the thrill of just beginning.</p>\r\n<p>If you could see the earth illuminated when you were in a place as dark as night, it would look to you more splendid than the moon.</p>', 1, '2013-10-02 13:54:03', '2013-10-02 13:30:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(200) NOT NULL,
  `Password` varchar(60) NOT NULL,
  `Name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`UserID`),
  UNIQUE KEY `Username` (`Username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`UserID`, `Username`, `Password`, `Name`) VALUES
(1, 'Flow', '$2y$11$Mlq7twZtUes8d9AdEYdwzOus3w6AuOGxqfJK/EYCuM8Be//m5gVQ.', 'Flow Digital'),
(2, 'Adam', '$2y$11$NnN7rlotPndMHP1/zTccbuayTULhIpM1R/k5Ww6LKliYCH1Kc8Oii', 'Adam');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;