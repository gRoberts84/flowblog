<?php
	/**
	 * Extended exception to make things tidier.
	 */
	class DBException extends Exception {
		public $innerException = null;
		public function __construct($message = null, $code = 0, $inner = null) {
			// Construct the underlying exception.
			parent::__construct($message, $code);
			// Store the inner exception.
			$this->innerException = $inner;
		}
	}
	/**
	 * Extended PDO class to make things tidier.
	 */
	class DB extends PDO {
		/**
		 * Constructor attempts to connect to the database.
		 */
		public function __construct() {
			// Include the database config.
			require_once(dirname(dirname(__FILE__)).'/config/db.php');
			// Validate the database config.
			if(!isset($DBConfig) || 
				empty($DBConfig['host']) || 
				empty($DBConfig['database']) || 
				empty($DBConfig['username']) || 
				empty($DBConfig['password'])) {
				throw new DBException('Missing or invalid database configuration');
			}
			try {
				// Construct the underlying PDO object and connect.
				parent::__construct(sprintf('mysql:host=%s;dbname=%s', $DBConfig['host'], $DBConfig['database']), $DBConfig['username'], $DBConfig['password']);
			} catch(Exception $e) {
				throw new DBException('Failed to connect to the database.', 0, $e);
			}
		}

		/**
		 * Prepares a database query using the supplied query and parameters.
		 * @param string $query Sql query to be executed
		 * @param array $params Array of params to be injected into the query.
		 * @return PDOStatement Returns an PDOStatement.
		 */
		public function preparedQuery($query, $params) {
			// Prepare the query.
			$stmt = $this->prepare($query);
			// Execute the query with the params.
			$stmt->execute($params);
			// Return the pdo statement.
			return $stmt;
		}
	}
?>