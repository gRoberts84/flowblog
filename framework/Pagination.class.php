<?php
	/**
	 * Pagination class to make things tidier.
	 */
	class Pagination {
		/**
		 * Generates a pagination control based on the
		 * supplied parameters.
		 * @param  string $url The base url in which the pagination links will point to.
		 * @param  integer $from The offset used to query the database.
		 * @param  integer $perPage The number of items per page.
		 * @param  integer $total The number of total rows available.
		 * @return string Returns an HTML control to help with pagination.
		 */
		public function generate($url, $from, $perPage, $total)
		{
			// Only continue if we have more rows than we need per page.
			if($total > $perPage)
			{
				// Define the pagination container and label.
				$output = '<nav class="pagination"><span>';
				$output .= sprintf('%s to %s of %s', $from + 1, ($from + $perPage <= $total ? $from + $perPage : $total), $total);
				$output .= '</span><ul>';
				// Determine how many pages we have.
				$pageCount = ceil($total / $perPage);
				$currentPage = 1;

				// Determine the current page based on the 
				// offset and how many items per page.
				$tmp = 0;
				while($tmp <= $total)
				{
					// Once we've hit our offset, stop counting.
					if($tmp == $from) 
						break;
					$tmp = $tmp + $perPage;
					$currentPage++;
				}

				// Only show the two previous and next page numbers where possible.
				$pageFrom = (($currentPage - 2) <= 2 ? 1 : $currentPage - 2);
				$pageTo = (($currentPage + 2) >= $pageCount ? $pageCount : $currentPage + 2);

				// Show the previous link, if we're not on the first page.
				if($currentPage > 1)
				{
					$output .= '<li><a href="' . $url . (($from - $perPage) == 0 ? '' : $from - $perPage) . '">&laquo;</a></li>';
				}
				// Loop through each of the pages we need to show and create a link.
				for($pageFrom = $pageFrom; $pageFrom <= $pageTo; $pageFrom++)
				{
					// If we're on the current page, don't show a link,
					// just show the number.
					if($pageFrom == $currentPage) 
						$output .= '<li><span>' . $pageFrom . '</span></li>';
					else
	                {
					    $output .= '<li><a href="' . $url  . ((($pageFrom - 1) * $perPage) == 0 ? '' : ($pageFrom - 1) * $perPage) . '">' . $pageFrom . '</a></li>';
	                }
				}
				// Show the next link if we're not on the last page.
				if($currentPage < $pageCount)
				{
				    $output .= '<li><a href="' . $url . ($from + $perPage) . '">&raquo;</a></li>';				
				}
				// Return the pagination fragment.
				return $output . '</ul></nav>';
			}
			return '';
		}
	}
?>