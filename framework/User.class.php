<?php
	/**
	 * Class representing a row in the User table.
	 */
	class User {
		public $UserID;
		public $Username;
		public $Password;
		public $Name;

		/**
		 * Securely generates a password hash using 
		 * BCRYPT and the password passed.
		 * @param  string $input Password to be hashed.
		 * @return string Hashed password.
		 */
		public function generatePassword($input) {
			// Check if we need to use a fallback.
			if(!function_exists('password_hash')) {
				require_once('Password.class.php');
			}
			// Hash the password.
			return password_hash($input, PASSWORD_BCRYPT, array('cost' => 11));
		}
		/**
		 * Validates a plain text password against
		 * the hashed version stored against the user.
		 * @param  string $input Plain text password.
		 * @return boolean Result of the password validation.
		 */
		public function validatePassword($input) {
			// Check if we need to use a fallback.
			if(!function_exists('password_hash')) {
				require_once('Password.class.php');
			}
			// Validate the password.
			return password_verify($input, $this->Password);
		}
	}
?>