<?php
	/**
	 * Class representing a row in the Article table.
	 */
	class Article {
		// Properties.
		public $ArticleID;
		public $Title;
		public $Content;
		public $Created;
		public $Published;
		public $LastUpdated;
		public $Removed;

		/**
		 * TODO! I must sanitise the content before outputting it
		 * to prevent any XSS injection attacks.
		 */
		
		/**
		 * Truncates the article content to only
		 * show the excerpt.
		 * @return string Excerpt of the content.
		 */
		public function getExcerpt() {
			// Ensure we have a break in the content.
			$breakPos = strpos($this->Content, '<hr class="break" />');
			// If we have a break, only return content up to the break.
			if($breakPos != false) {
				return substr($this->Content, 0, strpos($this->Content, '<hr class="break" />'));
			}
			// Otherwise return all of the content.
			return $this->Content;
		}
		/**
		 * 
		 * @param  integer $maxLength Maximum length of the title.
		 * @return string The title, truncated and ellipsis added if required.
		 */
		public function getTitle($maxLength = 100) {
			// Determine whether we need an ellipsis or not.
			$ellipsis = (strlen($this->Title) >= $maxLength ? '&#8230;' : '');
			// Truncate the title to the max length and add the ellipsis, if required.
			return substr($this->Title, 0, $maxLength) . $ellipsis;
		}
		/**
		 * Returns the content, stripping out the break element.
		 * @return string Content, without the break element.
		 */
		public function getContent() {
			return str_replace('<hr class="break" />', '', $this->Content);
		}
	}
?>