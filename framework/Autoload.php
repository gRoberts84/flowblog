<?php
	/**
	 * Setup the autoloader to help us with includes.
	 * @param  string $class Class name to include.
	 */
	function __autoLoad($class) {
		$path = "{$_SERVER['DOCUMENT_ROOT']}/framework/$class.class.php";
		if(file_exists($path)) {
			require_once($path);
		}
	}
?>