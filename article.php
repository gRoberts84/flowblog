<?php

	require_once('framework/Autoload.php');

	// Validate the user input before attempting to query the database.
	if(isset($_GET['ArticleID']) && is_numeric($_GET['ArticleID'])) {
		// Get the article id to fetch.
		$articleID = intval($_GET['ArticleID']);
		try {
			// Connect to the database.
			$con = new DB();
		} catch(DBException $e) {
			// Simple die message for now.
			die($e->getMessage() . ': ' . $e->innerException->getMessage());
		}

		// Setup a query to get the single article.
		$stmt = $con->preparedQuery('select Article.*, `user`.Name as PostedByName from Article inner join `User` on `User`.UserID = Article.UserID where Article.Removed is null and Article.Published is not null and Article.ArticleID = :ArticleID', array(':ArticleID' => $articleID), 'Article');
		// Get the row from the database, setup as an Article instance.
		$article = $stmt->fetchObject('Article');
		// Clear up some resources.
		$stmt->closeCursor();
		// Kill the connection.
		$con = null;
	}

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" type="text/css" href="assets/style.css">
</head>
<body>

	<div class="wrapper">
		<nav class="header-nav"><a id="admin" href="admin/index.php">Administrator</a></nav>
		<h1>Blog</h1>
		<?php if(empty($article)) : ?>
		<p class="no-items">The requested article could not be found.</p>
		<?php else : ?>
		<article class="first">
			<aside>Posted by <?php echo $article->PostedByName; ?> on <?php echo $article->Published; ?></aside>
			<h2><?php echo $article->Title; ?></h2>
			<?php echo $article->getContent(); ?>
		</article>
		<?php endif; ?>
	</div>
	
</body>
</html>