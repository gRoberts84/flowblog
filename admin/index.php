<?php

	require_once('../framework/Autoload.php');
	// Enable sessions.
	session_start();

	// Ensure the user is logged in.
	if(!isset($_SESSION['UserID'])) {
		header('location: login.php');
		return;
	}

	try {
		// Connect to the database.
		$con = new DB();
	} catch(DBException $e) {
		// Simple die message for now.
		die($e->getMessage() . ': ' . $e->innerException->getMessage());
	}

	// Check if we're performing an action.
	if(isset($_POST['Action'])) {
		// Get the action to perform.
		$action = $_POST['Action'];
		// Get the list of ArticleID's to perform the action again.
		$articleIDs = (isset($_POST['ArticleID']) ? $_POST['ArticleID'] : array());
		// Get the csrf token.
		$csrfToken = $_POST['csrfToken'];
		// Define an array to store any errors in.
		$errors = array();
		// Check if we have a valid request.
		if(empty($csrfToken) || $_SESSION['csrfToken'] != $csrfToken) {
			$errors[] = 'Invalid form submittion';
		}
		// Ensure we have an action.
		if(empty($action)) {
			$errors[] = 'Invalid action';
		}
		// Ensure we have some articles to perform the action against.
		if(empty($articleIDs)) {
			$errors[] = 'Nothing selected';
		}
		// Process the action.
		switch($action) {
			case 'Delete' : {
				// Define the delete SQL.
				$sql = 'update Article set Removed = current_timestamp()';
			} break;
			case 'Publish' : {
				// Define the publish SQL.
				$sql = 'update Article set Published = current_timestamp()';
			} break;
			case 'UnPublish' : {
				// Define the unpublish SQL.
				$sql = 'update Article set Published = null';
			} break;
			default : {
				// Invalid action found.
				$errors[] = 'Invalid action';
			} break;
		}
		// Only proceed if we have no errors.
		if(empty($errors)) {
			// Modify the query to include the article ids.
			$sql .= ' where ArticleID in (';
			// Define an array to store the params in.
			$params = array();
			// Loop through each article id, append
			// the article placeholder to the query
			// and append the article id to the params.
			foreach($articleIDs as $articleID) {
				$sql .= (empty($params) ? '' : ',')  . ':p' . $articleID;
				$params[':p' . $articleID] = $articleID;
			}
			$sql .= ')';
			// Execute the query.
			$con->preparedQuery($sql, $params);
		}
	}

	// Get where we should show articles from.
	$offset = (isset($_GET['Offset']) && is_numeric($_GET['Offset']) ? intval($_GET['Offset']) : 0);
	// Define how many articles per page we should show.
	$perPage = 5;

	// Prepare a query to query the articles, using SQL_CALC_FOUND_ROWS to return
	// the total amount of rows available should the limit not be used.
	$stmt = $con->prepare('select SQL_CALC_FOUND_ROWS Article.*, `user`.Name as PostedByName from Article inner join `User` on `User`.UserID = Article.UserID where Article.Removed is null order by Article.Published desc LIMIT :Offset, :PerPage');
	// The limit and offset parameters cannot be added
	// using the prepareQuery helper.
	$stmt->bindValue(':Offset', (int) $offset, PDO::PARAM_INT);
	$stmt->bindValue(':PerPage', (int) $perPage, PDO::PARAM_INT);
	// Execute the query.
	$stmt->execute();
	// Get all of the articles, setup as instances of the Article class.
	$articles = $stmt->fetchAll(PDO::FETCH_CLASS, 'Article');
	// Clear up some resources.
	$stmt->closeCursor();

	// Get the number of rows found in the above query.
	$stmt = $con->query('select FOUND_ROWS()');
	$total = intval($stmt->fetchColumn(0));
	// Clear up some resources.
	$stmt->closeCursor();

	// Kill the connection.
	$con = null;

	// Define the start/end page indexes.
	if($total > $perPage) {
		$start = $offset + $perPage;
		$end = ($start + $perPage >= $total ? $total : $start + $perPage); 
	}

	// Generate a new csrf token and assign it to the session.
	$csrfToken = md5(uniqid(rand(), TRUE));
	$_SESSION['csrfToken'] = $csrfToken;
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" type="text/css" href="../assets/style.css">
</head>
<body>
	<div class="wrapper">
		<nav class="header-nav"><a href="article.php">New article</a> | <a href="login.php?Logout">Logout</a></nav>
		<h1>Blog admin</h1>
		<?php if(!empty($errors)) : ?>
		<ul class="error">
			<?php foreach($errors as $Error) : ?>
			<li><?php echo $Error; ?></li>
			<?php endforeach; ?>
		</ul>
		<?php endif; ?>
		<?php if(empty($articles)) : ?>
		<p>There are no articles currently available to view.</p>
		<?php else : ?>
		<form method="post">
			<fieldset>
				<input type="hidden" name="csrfToken" value="<?php echo $csrfToken; ?>">
				<table class="articles">
					<thead>
						<tr>
							<td class="article-check">&nbsp;</td>
							<td>Title</td>
							<td>Created</td>
							<td>By</td>
							<td>Updated</td>
							<td>Published</td>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="2">
								<label for="Action">Action: </label>
								<select name="Action" id="Action">
									<option>Select</option>
									<option value="Delete">Delete</option>
									<option value="Publish">Publish</option>
									<option value="UnPublish">UnPublish</option>
								</select> <input type="submit" name="PerformAction" value="Go">
							</td>
							<td colspan="4" align="right">
								<?php 
									$pagination = new Pagination();
									echo $pagination->generate('index.php?Offset=', $offset, $perPage, $total); 
								?>
							</td>
						</tr>
					</tfoot>
					<tbody>
						<?php foreach($articles as $article) : ?>
						<tr>
							<td><input type="checkbox" name="ArticleID[]" value="<?php echo $article->ArticleID; ?>"></td>
							<td><a href="article.php?ArticleID=<?php echo $article->ArticleID; ?>"><?php echo $article->getTitle(); ?></a></td>
							<td><?php echo $article->Created; ?></td>
							<td><?php echo $article->PostedByName; ?></td>
							<td><?php echo (empty($article->LastUpdated) ? 'N/A' : $article->LastUpdated); ?></td>
							<td><?php echo (empty($article->Published) ? 'N/A' : $article->Published); ?></td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</fieldset>
		</form>
		<?php endif; ?>
	</div>
</body>
</html>