<?php

	require_once('../framework/Autoload.php');
	// Enable sessions.
	session_start();
	
	// If we want to logout, destroy the session.
	if(isset($_GET['Logout'])) {
		unset($_SESSION['UserID']);
	}

	// Check for a login attempt.
	if(!empty($_POST) && !empty($_POST['Login'])) {
		// Define an array to store any errors in.
		$errors = array();
		// Get the post variables.
		$username = $_POST['Username'];
		$password = $_POST['Password'];
		$csrfToken = $_POST['csrfToken'];
		// Ensure we have a valid request.
		if(empty($csrfToken) || $_SESSION['csrfToken'] != $csrfToken) {
			$errors[] = 'Invalid form submittion';
		}
		// Ensure we have a username.
		if(empty($username)) {
			$errors[] = 'Username is required';
		}
		// Ensure we have a password.
		if(empty($password)) {
			$errors[] = 'Password is required';
		}
		// Continue if we don't have any errors.
		if(empty($errors)) {
			try {
				// Connect to the database.
				$con = new DB();
			} catch(DBException $e) {
				// Simple die message for now.
				die($e->getMessage() . ': ' . $e->innerException->getMessage());
			}

			// Setup a query to get the user.
			$stmt = $con->preparedQuery('select * from `User` where Username = :Username', array(':Username' => $username), 'User');
			// Get the row from the database, setup as a User object.
			$user = $stmt->fetchObject('User');
			// Clear up some resources.
			$stmt->closeCursor();
			// Kill the connection.
			$con = null;
			// Attempt to validate the user supplied password
			// against the one in the database.
			if($user != null && $user->validatePassword($password)) {
				// Set the user id and redirect to the admin index.
				$_SESSION['UserID'] = $user->UserID;
				header('location: index.php');
				return;
			} else {
				// Tell the user the credentials failed.
				$errors[] = 'Invalid username and/or password';
			}
		}
	}
	// Generate a new csrf token and assign it to the session.
	$csrfToken = md5(uniqid(rand(), TRUE));
	$_SESSION['csrfToken'] = $csrfToken;

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" type="text/css" href="../assets/style.css">
</head>
<body>
	<div class="wrapper">
		<h1>Blog admin</h1>
		<form method="post">
			<fieldset>
				<input type="hidden" name="csrfToken" value="<?php echo $csrfToken; ?>">
				<p>Enter your login details below:</p>
				<?php if(!empty($errors)) : ?>
				<ul class="error">
					<?php foreach($errors as $Error) : ?>
					<li><?php echo $Error; ?></li>
					<?php endforeach; ?>
				</ul>
				<?php endif; ?>
				<label for="Username">Username</label>
				<input type="text" name="Username" id="Username">
				<br />
				<label for="Password">Password</label>
				<input type="password" name="Password" id="Password">
				<br />
				<br />
				<input type="submit" name="Login" value="Login">
			</fieldset>
		</form>
	</div>
</body>
</html>