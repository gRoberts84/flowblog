<?php

	require_once('../framework/Autoload.php');
	// Enable sessions.
	session_start();

	// Ensure the user is logged in.
	if(!isset($_SESSION['UserID'])) {
		header('location: login.php');
		return;
	}

	/**
	 * Helper function to get a value from the post
	 * array or return a default value.
	 * @param string $key The post key to get.
	 * @param mixed $default The default value to return if the required key does not exist.
	 * @return mixed Either the value of the $key post variable or default.
	 */
	function getPostValue($key, $default) {
		// Check if the required post value exists.
		if(empty($_POST) || !isset($_POST[$key])) {
			return $default;
		}
		// Return the post value.
		return $_POST[$key];
	}

	try {
		// Connect to the database.
		$con = new DB();
	} catch(DBException $e) {
		// Simple die message for now.
		die($e->getMessage() . ': ' . $e->innerException->getMessage());
	}

	// Ensure that if we have an article id, it's valid.
	if(isset($_GET['ArticleID']) && !is_numeric($_GET['ArticleID'])) {
		$errors = array();
		$errors[] = 'Invalid article id';
	}

	// Check if we are editing an article or not.
	if(empty($_POST) && isset($_GET['ArticleID']) && is_numeric($_GET['ArticleID'])) {
		// Get the article id to edit.
		$articleID = intval($_GET['ArticleID']);
		// Setup a query to get the article.
		$stmt = $con->preparedQuery('select Article.*, `user`.Name as PostedByName from Article inner join `User` on `User`.UserID = Article.UserID where Article.Removed is null and Article.ArticleID = :ArticleID', array(':ArticleID' => $articleID), 'Article');
		// Get the row from the database, setup as an Article object.
		$article = $stmt->fetchObject('Article');
		// Clear up some resources.
		$stmt->closeCursor();
		// Check if the requested article exists.
		if(empty($article)) {
			$errors[] = 'Invalid article id';
			unset($article);
		}
	}
	// Check if we've clicked save.
	if(isset($_POST['Save'])) {
		// Define an array to store any errors in.
		$errors = array();
		// Get the post variables.
		$title = $_POST['Title'];
		$published = $_POST['Published'];
		// Attempt to parse the published date.
		$publishedDate = strtotime($published);
		$content = $_POST['Content'];

		// Ensure we have a valid request.
		if(!isset($_POST['csrfToken']) || $_POST['csrfToken'] != $_SESSION['csrfToken']) {
			$errors[] = 'Invalid form submittion';
		}
		// Ensure we have a title.
		if(empty($title)) {
			$errors[] = 'Title is required';
		}
		// Ensure that the published date is valid, if supplied.
		if(!empty($published) && !is_numeric($publishedDate)) {
			$errors[] = 'Published date is invalid';
		}
		// Ensure we have some content.
		if(empty($content)) {
			$errors[] = 'Content is required';
		}

		// Only proceed if we don't have any errors.
		if(empty($errors)) {
			// Get the article id if we're editing, otherwise -1.
			$articleID = (isset($_GET['ArticleID']) && is_numeric($_GET['ArticleID']) ? intval($_GET['ArticleID']) : -1);
			// We're editing.
			if($articleID != -1) {
				// Prepare and execute the update query.
				$con->preparedQuery('update Article set Title = :Title, Published = :Published, Content = :Content where ArticleID = :ArticleID', array(
					':Title' => $title,
					':Published' => (empty($published) ? null : date('Y-m-d H:i:s', $publishedDate)),
					':Content' => $content,
					':ArticleID' => $articleID
				));
			} else {
				// Prepare and execute the insert query.
				$con->preparedQuery('insert into Article (Title, Published, UserID, Content) values (:Title, :Published, :UserID, :Content)', array(
					':Title' => $title,
					':Published' => (empty($published) ? null : date('Y-m-d H:i:s', $publishedDate)),
					':UserID' => $_SESSION['UserID'],
					':Content' => $content
				));
			}
			// Kill the connection.
			$con = null;
			// Redirect to the start.
			header('location: index.php');
			return;
		}
	}
	// Check if we're removing the article.
	if(isset($_POST['Remove'])) {
		// Get the article id if we're removing, otherwise -1.
		$articleID = (isset($_GET['ArticleID']) && is_numeric($_GET['ArticleID']) ? intval($_GET['ArticleID']) : -1);
		// Ensure we have a valid request.
		if(!isset($_POST['csrfToken']) || $_POST['csrfToken'] != $_SESSION['csrfToken']) {
			$errors[] = 'Invalid form submittion';
		}
		// Ensure we have an article to remove.
		if($articleID == -1) {
			$errors[] = 'Invalid article id';
		}
		// Only proceed if we don't have any errors.
		if(empty($errors)) {
			// Update the article.
			$con->preparedQuery('update Article set Removed = current_timestamp() where ArticleID = :ArticleID', array(
				':ArticleID' => $articleID
			));
			// Kill the connection.
			$con = null;
			// Redirect to the start.
			header('location: index.php');
			return;
		}
	}
	// Kill the connection.
	$con = null;

	// Generate a new csrf token and assign it to the session.
	$csrfToken = md5(uniqid(rand(), TRUE));
	$_SESSION['csrfToken'] = $csrfToken;
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" type="text/css" href="../assets/style.css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="../assets/tinymce/jquery.tinymce.js"></script>
	<script>
		$(function() {
			// Setup tinymce on the textarea.
			$('textarea').tinymce({
                script_url : '../assets/tinymce/tiny_mce.js',
                theme : 'advanced',
                theme_advanced_buttons1 : 'bold,italic,underline,separator,bullist,numlist,separator,undo,redo,separator,seperator,styleselect,seperator,pageBreak',
                theme_advanced_buttons2 : '',
                theme_advanced_buttons3 : '',
                theme_advanced_styles : 'Article break=break',
                setup : function(ed) {
                	// Add a custom button that'll insert our page-break
                	// element <hr class="break" />
				    ed.addButton('pageBreak', {
					    title : 'Page break',
					    'class' : 'mce_hr',
					    onclick : function() {
					        ed.focus();
					        ed.selection.setContent('<hr class="break" />');
					    }
				    });
				}
			});
		});
	</script>
	<style type="text/css">
		textarea {
			width: 100%;
		}
	</style>
</head>
<body>
	<div class="wrapper">
		<nav class="header-nav"><a href="index.php">Articles</a> | <a href="login.php?Logout">Logout</a></nav>
		<h1>Blog admin</h1>
		<?php if(!empty($errors)) : ?>
		<ul class="error">
			<?php foreach($errors as $Error) : ?>
			<li><?php echo $Error; ?></li>
			<?php endforeach; ?>
		</ul>
		<?php endif; ?>
		<form method="post">
			<fieldset>
				<input type="hidden" name="csrfToken" value="<?php echo $csrfToken; ?>">
				<label for="Title">Title</label>
				<input type="text" name="Title" id="Title" value="<?php echo getPostValue('Title', (isset($article) ? $article->Title : '')); ?>">
				<br />
				<label for="Published">Published</label>
				<input type="text" class="datePicker" name="Published" id="Published" value="<?php echo getPostValue('Published', (isset($article) ? $article->Published : '')); ?>">
				<br />
				<label for="Content">Content</label>
				<textarea name="Content" id="Content"><?php echo getPostValue('Content', (isset($article) ? $article->Content : '')); ?></textarea>
				<br />
				<input type="submit" name="Save" value="Save">
				<?php if(isset($article)) : ?>
				<input type="submit" name="Remove" value="Remove">
				<?php endif; ?>
				<a href="index.php">Cancel</a>
			</fieldset>
		</form>
	</div>
</body>
</html>