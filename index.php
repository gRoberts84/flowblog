<?php

	require_once('framework/Autoload.php');
	
	try {
		// Connect to the database.
		$con = new DB();
	} catch(DBException $e) {
		// Simple die message for now.
		die($e->getMessage() . ': ' . $e->innerException->getMessage());
	}
	// Get all active and published articles, ordered by their published date.
	$articles = $con->query('select Article.*, `user`.Name as PostedByName from Article inner join `User` on `User`.UserID = Article.UserID where Article.Removed is null and Article.Published is not null order by Article.Published desc');
	// Tell PDO to setup a new Article instance for each row.
	$articles->setFetchMode(PDO::FETCH_INTO, new Article);
	// Kill the connection.
	$con = null;

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" type="text/css" href="assets/style.css">
</head>
<body>

	<div class="wrapper">
		<nav class="header-nav"><a id="admin" href="admin/index.php">Administrator</a></nav>
		<h1>Blog</h1>
		<?php if(empty($articles)) : ?>
		<p>There are no articles currently available to view.</p>
		<?php else : ?>
		<?php 
			// Use a simple variable to determine the
			// first row.
			$first = true; 
			foreach($articles as $article) : ?>
		<article class="<?php echo ($first ? 'first' : ''); ?>">
			<aside>Posted by <?php echo $article->PostedByName; ?> on <?php echo $article->Published; ?></aside>
			<h2><a href="article.php?ArticleID=<?php echo $article->ArticleID; ?>"><?php echo $article->getTitle(); ?></a></h2>
			<?php echo $article->getExcerpt(); ?>
			<p><a href="article.php?ArticleID=<?php echo $article->ArticleID; ?>">Read more</a></p>
		</article>
		<?php 
			// Clear the first flag.
			$first = false; 
			endforeach; ?>
		<?php endif; ?>
	</div>
	
</body>
</html>